package core;

import java.io.Serializable;

public enum Unit implements Serializable{
	Millilitre,
	Litre,
	Gramme,
	Spoon,
	TeaSpoon,
	Sachet,
	None,
	Slice,
	Pinch,
	NoQuantity;
	
	public String toString(){
		switch(this){
			case None :
				return I18n.tr("None");
			case Millilitre :
				return I18n.tr("Millilitre");
			case Litre :
				return I18n.tr("Litre");
			case Gramme :
				return I18n.tr("Gramme");
			case TeaSpoon :
				return I18n.tr("TeaSpoon");
			case Sachet :
				return I18n.tr("Sachet");
			case Spoon :
				return I18n.tr("Spoon");
			case Slice :
				return I18n.tr("Slice");
			case Pinch :
				return I18n.tr("Pinch");
			case NoQuantity :
				return I18n.tr("NoQuantity");
			default:
				return null;
		}
	}
	
	public int toInt(){
		switch(this){
			case None :
				return 0;
			case Millilitre :
				return 1;
			case Litre :
				return 2;
			case Gramme :
				return 3;
			case TeaSpoon :
				return 4;
			case Sachet :
				return 5;
			case Spoon :
				return 6;
			case Slice :
				return 7;
			case Pinch :
				return 8;
			case NoQuantity :
				return 9;
			default:
				return -1;
		}
	}
}
