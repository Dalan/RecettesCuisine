package core;

import java.io.Serializable;

public enum Baking implements Serializable {
	None,
	Oven,
	Frying_pan,
	Saucepan,
	Pressure_cooker,
	Various;
	
	public String toString(){
		switch(this){
			case None :
				return I18n.tr("None");
			case Oven :
				return I18n.tr("Oven");
			case Frying_pan :
				return I18n.tr("Frying_pan");
			case Saucepan :
				return I18n.tr("Saucepan");
			case Pressure_cooker :
				return I18n.tr("Pressure_cooker");
			case Various :
				return I18n.tr("Various");
			default:
				return null;
		}
	}
	
	public int toInt(){
		switch(this){
			case None :
				return 0;
			case Oven :
				return 1;
			case Frying_pan :
				return 2;
			case Saucepan :
				return 3;
			case Pressure_cooker :
				return 4;
			case Various :
				return 5;
			default:
				return -1;
		}
	}
}
