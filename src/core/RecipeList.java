package core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class RecipeList implements Serializable, Cloneable {

	private static final long serialVersionUID = -4579076863400484624L;
	
	protected List<Recipe> recipes;
	protected String name;

	/**
	 * 
	 */
	public RecipeList() {
		recipes = new ArrayList<>();
	}
	
	@Override
	public Object clone() {
		RecipeList rl = new RecipeList();
		rl.name = name;
		for (Recipe r : recipes)
			rl.recipes.add((Recipe) r.clone());
		return rl;
	}

	/**
	 * @throws IOException 
	 * 
	 */
	
	public void save() throws IOException{
		FileOutputStream f = new FileOutputStream(name);
		ObjectOutputStream out = new ObjectOutputStream(f);
		out.writeObject(recipes);
		out.close();
	}
	
	public void save(String n) throws IOException{
		name = n;
		save();
	}
	
	@SuppressWarnings("unchecked")
	protected void open() throws IOException{
		recipes.clear();
		FileInputStream f = new FileInputStream(name);
		ObjectInputStream in = new ObjectInputStream(f);
		try {
			recipes = (List<Recipe>)in.readObject();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		in.close();
	}
	
	public void open(String n) throws IOException{
		name = n;
		open();
	}
	
	public void exportToHtml(String s) throws IOException{
		FileWriter fw = new FileWriter(s);
		PrintWriter pw = new PrintWriter(fw);
		for (Recipe r : recipes)
			pw.println(r.toHTMLString());
		pw.close();
	}
	
	public void exportToMarkdown(String s) throws IOException{
		FileWriter fw = new FileWriter(s);
		PrintWriter pw = new PrintWriter(fw);
		for (Recipe r : recipes)
		{
			pw.print(r.toMarkdownString());
			pw.print("---" + System.lineSeparator() + System.lineSeparator() );
		}
		pw.close();
	}
	
	public void exportToXml(String s) throws IOException{
		Document doc = new Document();
		Element root = new Element("recipe_list");
		doc.setRootElement(root);
		
		root.addContent(new Element("version").setText("1.0"));
		
		for (Recipe r:recipes)
			root.addContent(r.toXmlElement());
		
		XMLOutputter xmlOutput = new XMLOutputter();
		xmlOutput.setFormat(Format.getPrettyFormat());
		xmlOutput.output(doc, new FileWriter(s));
	}
	
	public void exportToJson(String s) throws IOException {
		for (Recipe r:recipes) {
			File dir = new File(s + "/" + r.getName());
			dir.mkdir();
			r.exportToJson(dir.toString() + "/recipe.json");
		}
	}
	
	public void exportToPdf(String s) throws IOException{
		PDDocument doc = new PDDocument();
		
		for (Recipe r:recipes){
			for (PDPage p: r.toPdfPage(doc))
				doc.addPage(p);
		}
		
		PDDocumentInformation info = new PDDocumentInformation();
		info.setAuthor(System.getProperty("user.name"));
		info.setProducer("PDFBox");
		info.setCreator("RecettesCuisine");
		info.setCreationDate(Calendar.getInstance());
		info.setTitle(I18n.tr("RecipeBook"));
		doc.setDocumentInformation(info);
		
		doc.save(s);
		doc.close();
	}
	
	public void clear(){
		recipes.clear();
		name = null;
	}

	/**
	 * 
	 */
	
	/**
	 * @return the recipes
	 */
	public List<Recipe> getRecipes() {
		return recipes;
	}

	/**
	 * @param recipes the recipes to set
	 */
	public void setRecipes(List<Recipe> recipes) {
		this.recipes = recipes;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	


	/**
	 * 
	 */

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((recipes == null) ? 0 : recipes.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RecipeList other = (RecipeList) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (recipes == null) {
			if (other.recipes != null)
				return false;
		} else if (!recipes.equals(other.recipes))
			return false;
		return true;
	}
	
	public boolean sameRecipes(RecipeList rl){
		if (recipes.size() != rl.recipes.size())
			return false;
		for (int i=0 ; i<recipes.size() ; i++){
			if (! recipes.get(i).equals(rl.recipes.get(i)))
				return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sf = new StringBuffer();
		sf.append(name + "\n\n");
		for (Recipe r: recipes)
			sf.append(r.toString() + "\n\n");
		return sf.toString();
	}

}
