package core;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.io.FileWriter;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.jdom2.Attribute;
import org.jdom2.Element;
import org.json.JSONObject;

public class Recipe implements Serializable, Cloneable {
	private String name;
	private int time=0;
	private List<Ingredient> ingredients;
	private Baking baking = Baking.None;
	private List<String> actions;
	private List<String> utensils;
	private Difficulty difficulty;
	private int grade=0;
	private int nb_person=0;
	
	private static final long serialVersionUID = 8267746199251258267L;

	/**
	 * 
	 */
	public Recipe(String n) {
		name = n;
		ingredients = new ArrayList<>();
		actions = new ArrayList<>();
		utensils = new ArrayList<>();
	}
	
	@Override
	public Object clone() {
		Recipe r = new Recipe(name);
		r.time = time;
		r.baking = baking;
		r.grade = grade;
		r.difficulty = difficulty;
		r.nb_person = nb_person;
		for (Ingredient i : ingredients)
			r.ingredients.add((Ingredient) i.clone());
		for (String s : actions)
			r.actions.add(s);
		for (String s : utensils)
			r.utensils.add(s);
		return r;
	}
	
	public Element toXmlElement(){
		Element re = new Element("recipe");
		
		re.addContent(new Element("name").setText(name));
		re.addContent(new Element("time").setText((Integer.valueOf(time)).toString()));
		re.addContent(new Element("difficulty").setText((Integer.valueOf(difficulty.toInt())).toString()));
		re.addContent(new Element("grade").setText((Integer.valueOf(grade)).toString()));
		re.addContent(new Element("nb_person").setText((Integer.valueOf(nb_person)).toString()));
		re.addContent(new Element("baking").setText((Integer.valueOf(baking.toInt())).toString()));
		
		Element ingredients_list = new Element("ingredients_list");
		re.addContent(ingredients_list);
		for (Ingredient i : ingredients)
			ingredients_list.addContent(i.toXmlElement());
		
		Element ustensil_list = new Element("ustensils_list");
		re.addContent(ustensil_list);
		for (String s : utensils){
			Element ustensil = new Element("ustensil");
			ustensil.setText(s);
			ustensil_list.addContent(ustensil);
		}
		
		Element actions_list = new Element("actions_list");
		re.addContent(actions_list);
		Integer i = 0;
		for (String s : actions){
			Element action = new Element("action");
			action.setAttribute(new Attribute("order", i.toString()));
			action.setText(s);
			actions_list.addContent(action);
			i++;
		}
		
		return re;
	}
	
	public List<PDPage> toPdfPage(PDDocument doc) throws IOException{
		final float width = PDRectangle.A4.getWidth();
		final float height = PDRectangle.A4.getHeight();
		
		final float margin = 40;

		PdfTextWriter writer = new PdfTextWriter(height);
		
		final PDFont font_title = PDType1Font.TIMES_BOLD;
		final int font_size = 12;
		InputStream input = getClass().getClassLoader().getResourceAsStream(
                "org/apache/pdfbox/resources/ttf/LiberationSans-Regular.ttf");
        PDType0Font font = PDType0Font.load(doc, input);
		
		writer.addPage(doc);
		
		writer.begin();
		writer.setFont(font_title, 28);
		writer.newLineAtOffset((width - writer.getStringWidth(name))/2, height - writer.getLineHeight() - margin);
		writer.write(name);
		writer.end();
		

		writer.begin();
		writer.setFont(font, font_size);
		writer.newLineAtOffset(margin, height - 120);
		writer.write(I18n.tr("PreparationTime") + time  + " "+ I18n.tr("Minutes"));
		writer.newLine();
		writer.write(I18n.tr("Difficulty") + difficulty);
		writer.newLine();
		writer.write(I18n.tr("Baking") + baking);
		writer.newLine();
		writer.write(I18n.tr("Grade") + grade);

		writer.newLine();
		writer.newLine();
		writer.write(I18n.tr("Ingredients1") + nb_person + I18n.tr("Ingredients2"));
		for (Ingredient ing : ingredients){
			writer.newLine();
			writer.write("    \u2022 " + ing.toString());
		}
		
		if (! utensils.isEmpty()){
			writer.newLine();
			writer.newLine();
			writer.write(I18n.tr("Ustensils"));
			for(String s : utensils){
				writer.newLine();
				writer.write("    \u2022 " + s);
			}
		}
		
		writer.newLine();
		writer.newLine();
		writer.write(I18n.tr("Preparation"));
		for (String a : actions){
			boolean first_line = true;
			int size = a.length();
			StringBuffer sb = new StringBuffer(a);
			
			if (writer.getPosition() < margin)
			{
				writer.end();
				writer.addPage(doc);
				writer.setFont(font, font_size);
				writer.begin();
				writer.newLineAtOffset(margin, height - margin - writer.getLineHeight());
			}
			else
				writer.newLine();
			
			while (sb.length() > 0){
				float text_width = writer.getStringWidth("      " + sb.substring(1, size));
				if (text_width < width - 2*margin && (sb.charAt(size-1) == ' ' || size == sb.length())){
					if (first_line){
						first_line = false;
						writer.write("    \u2022 " + sb.substring(0, size));
					}else{
						writer.newLine();
						writer.write("      " + sb.substring(0, size));
					}
					sb.delete(0, size);
					size = sb.length();
				} else
					size--;
			}
		}
		

		writer.end();
		writer.close();
		
		return writer.getPages();
	}
	
	public void exportToJson(String filename) throws IOException {
		FileWriter fw = new FileWriter(filename);
		JSONObject json = new JSONObject();
		json.put("@context", "https://schema.org");
		json.put("@type", "Recipe");
		json.put("author", System.getProperty("user.name"));
		json.put("name", name);
		json.put("recipeYield", nb_person);
		json.put("totalTime", "PT" + time / 60 + "H" + time %60 + "M");
		ArrayList<String> str_ingredients = new ArrayList<>();
		for (Ingredient ing: ingredients)
			str_ingredients.add(ing.toString());
		json.put("recipeIngredient", str_ingredients);
		json.put("cookingMethod", baking.toString());
		json.put("recipeInstructions", actions);
		json.put("tool", utensils);
		JSONObject json_rating = new JSONObject();
		json_rating.put("worstRating", 0);
		json_rating.put("bestRating", 10);
		json_rating.put("ratingValue", grade);
		JSONObject json_review = new JSONObject();
		json_review.put("reviewRating", json_rating);
		json_review.put("reviewBody", I18n.tr("difficulty_json") + difficulty.toString());
		json.put("review", json_review);
		json.write(fw);
		fw.close();
	}
	
	
	/**
	 * 
	 */

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the time
	 */
	public int getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(int time) {
		this.time = time;
	}

	/**
	 * @return the ingredients
	 */
	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	/**
	 * @param ingredients the ingredients to set
	 */
	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	/**
	 * @return the baking
	 */
	public Baking getBaking() {
		return baking;
	}

	/**
	 * @param baking the baking to set
	 */
	public void setBaking(Baking baking) {
		this.baking = baking;
	}

	/**
	 * @return the actions
	 */
	public List<String> getActions() {
		return actions;
	}

	/**
	 * @param actions the actions to set
	 */
	public void setActions(List<String> actions) {
		this.actions = actions;
	}

	/**
	 * @return the utensils
	 */
	public List<String> getUtensils() {
		return utensils;
	}

	/**
	 * @param utensils the utensils to set
	 */
	public void setUtensils(List<String> utensils) {
		this.utensils = utensils;
	}


	/**
	 * @return the difficulty
	 */
	public Difficulty getDifficulty() {
		return difficulty;
	}


	/**
	 * @param difficulty the difficulty to set
	 */
	public void setDifficulty(Difficulty difficulty) {
		this.difficulty = difficulty;
	}


	/**
	 * @return the grade
	 */
	public int getGrade() {
		return grade;
	}


	/**
	 * @param grade the grade to set
	 */
	public void setGrade(int grade) {
		this.grade = grade;
	}


	public int getNb_person() {
		return nb_person;
	}

	public void setNb_person(int nb_person) {
		this.nb_person = nb_person;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((actions == null) ? 0 : actions.hashCode());
		result = prime * result + ((baking == null) ? 0 : baking.hashCode());
		result = prime * result + ((difficulty == null) ? 0 : difficulty.hashCode());
		result = prime * result + grade;
		result = prime * result + ((ingredients == null) ? 0 : ingredients.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + time;
		result = prime * result + nb_person;
		result = prime * result + ((utensils == null) ? 0 : utensils.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Recipe other = (Recipe) obj;
		if (actions == null) {
			if (other.actions != null)
				return false;
		} else if (!actions.equals(other.actions))
			return false;
		if (baking != other.baking)
			return false;
		if (difficulty != other.difficulty)
			return false;
		if (grade != other.grade)
			return false;
		if (ingredients == null) {
			if (other.ingredients != null)
				return false;
		} else if (!ingredients.equals(other.ingredients))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (time != other.time)
			return false;
		if (nb_person != other.nb_person)
			return false;
		if (utensils == null) {
			if (other.utensils != null)
				return false;
		} else if (!utensils.equals(other.utensils))
			return false;
		return true;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sf = new StringBuffer();
		sf.append(name + "\n");
		sf.append(I18n.tr("PreparationTime") + time  + " "+ I18n.tr("Minutes") + "\n");
		sf.append(I18n.tr("Difficulty") + difficulty + "\n");
		sf.append(I18n.tr("Grade") + grade + "\n");
		sf.append(I18n.tr("Baking") + baking + "\n");
		
		sf.append(I18n.tr("Ingredients1") + nb_person + I18n.tr("Ingredients2") + "\n");
		for(Ingredient i : ingredients){
			sf.append(" - " + i.toString() + "\n");
		}
		
		if (! utensils.isEmpty()){
			sf.append(I18n.tr("Ustensils") + "\n");
			for(String s : utensils){
				sf.append(" - " + s + "\n");
			}
		}
		
		sf.append(I18n.tr("Preparation") + "\n");
		for(String s : actions){
			sf.append(" - " + s + "\n");
		}
		return sf.toString();
	}
	
	public String toHTMLString() {
		StringBuffer sf = new StringBuffer();
		
		sf.append("<html>");
		
		sf.append("<b><font size=\"6\"><center>" + name + "</center></font></b>");
		
		sf.append("<p>" + I18n.tr("PreparationTime") + time  + " "+ I18n.tr("Minutes") + "<br>");
		sf.append(I18n.tr("Difficulty") + difficulty + "<br>");
		sf.append(I18n.tr("Grade") + grade + "<br>");
		sf.append(I18n.tr("Baking") + baking + "</p>");
		
		sf.append("<p>" + I18n.tr("Ingredients1") + nb_person + I18n.tr("Ingredients2") + "<br><ul>");
		for(Ingredient i : ingredients){
			sf.append("<li>" + i.toString() + "</li>");
		}
		sf.append("</ul></p>");
		
		if (! utensils.isEmpty()){
			sf.append("<p>" + I18n.tr("Ustensils") + "<br><ul>");
			for(String s : utensils){
				sf.append("<li>" + s + "</li>");
			}
			sf.append("</ul></p>");
		}
		
		sf.append("<p>" + I18n.tr("Preparation") + "<br><ul>");
		for(String s : actions){
			sf.append("<li>" + s + "</li>");
		}
		sf.append("</ul></p>");
		
		sf.append("</html>");
		
		return sf.toString();
	}
	
	public String toMarkdownString(){
		StringBuffer sf = new StringBuffer();
		
		sf.append("#" + name + System.lineSeparator() + System.lineSeparator());
		
		sf.append(I18n.tr("PreparationTime") + time  + " "+ I18n.tr("Minutes") + System.lineSeparator());
		sf.append(I18n.tr("Difficulty") + difficulty  + System.lineSeparator());
		sf.append(I18n.tr("Grade") + grade  + System.lineSeparator());
		sf.append(I18n.tr("Baking") + baking + System.lineSeparator() + System.lineSeparator() );
		
		sf.append(I18n.tr("Ingredients1") + nb_person + I18n.tr("Ingredients2") + System.lineSeparator() + System.lineSeparator());
		for(Ingredient i : ingredients){
			sf.append("* " + i.toString() + System.lineSeparator());
		}
		sf.append(System.lineSeparator());
		
		if (! utensils.isEmpty()){
			sf.append(I18n.tr("Ustensils") + System.lineSeparator() + System.lineSeparator());
			for(String s : utensils){
				sf.append("* " + s  + System.lineSeparator());
			}
			sf.append(System.lineSeparator());
		}
		
		sf.append(I18n.tr("Preparation") + System.lineSeparator() + System.lineSeparator());
		for(String s : actions){
			sf.append("* " + s  + System.lineSeparator());
		}
		sf.append(System.lineSeparator());
		
		return sf.toString();
	}
	
	private class PdfTextWriter{
		private float position;
		private float leading;
		private List<PDPage> pages;
		private PDPageContentStream content = null;
		private PDFont font;
		float font_size;
		
		public PdfTextWriter(float h) {
			position = h;
			pages = new ArrayList<>();
		}
		
		public void addPage(PDDocument doc) throws IOException {
			close();
			
			PDPage page = new PDPage(PDRectangle.A4);
			pages.add(page);
			content = new PDPageContentStream(doc, page);
		}
		
		public void begin() throws IOException {
			content.beginText();
		}
		
		public void end() throws IOException {
			content.endText();
		}
		
		public void setFont(PDFont f, float fontSize) throws IOException {
			content.setFont(f, fontSize);
			content.setLeading(fontSize + 1);
			font = f;
			font_size = fontSize;
			leading = getLineHeight();
		}
		
		public float getLineHeight() {
			return font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * (font_size +1) * 0.82F;
		}
		
		public float getStringWidth(String text) throws IOException{
			return font.getStringWidth(text) / 1000 * font_size;
		}
		
		public void newLine() throws IOException{
			position -= leading;
			content.newLine();
		}
		
		public void newLineAtOffset(float tx, float ty) throws IOException{
			content.newLineAtOffset(tx, ty);
			position = ty;
		}
		
		public void write(String text) throws IOException{
			content.showText(text);
		}
		
		public float getPosition() {
			return position;
		}
		
		public void close() throws IOException{
			if (content != null)
			{
				content.close();
			}
		}
		
		public List<PDPage> getPages() throws IOException{
			return pages;
		}
	}
}
