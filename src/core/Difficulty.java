package core;

import java.io.Serializable;

public enum Difficulty implements Serializable {
	VeryEasy,
	Easy,
	Medium,
	Hard,
	VeryHard;

	public String toString(){
		switch(this){
			case VeryEasy :
				return I18n.tr("VeryEasy");
			case Easy :
				return I18n.tr("Easy");
			case Medium :
				return I18n.tr("Medium");
			case Hard :
				return I18n.tr("Hard");
			case VeryHard :
				return I18n.tr("VeryHard");
			default:
				return null;
		}
	}
	
	public int toInt(){
		switch(this){
			case VeryEasy :
				return 0;
			case Easy :
				return 1;
			case Medium :
				return 2;
			case Hard :
				return 3;
			case VeryHard :
				return 4;
			default:
				return -1;
		}
	}
}
