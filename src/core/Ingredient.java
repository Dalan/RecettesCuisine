package core;

import java.io.Serializable;

import org.jdom2.Element;

public class Ingredient implements Serializable, Cloneable {
	private double quantity;
	private Unit unit;
	private String name;
	private String comment;

	private static final long serialVersionUID = -6929041792870716120L;
	
	/**
	 * 
	 */
	public Ingredient(double q, Unit u, String n, String c) {
		quantity = q;
		unit = u;
		name = n;
		comment = c;
	}
	
	public Ingredient() {
		this(0,Unit.Gramme,"","");
	}
	
	public Ingredient(String n) {
		this(0,Unit.Gramme,n,"");
	}
	
	
	/**
	 * 
	 */
	@Override
	public String toString() {
		StringBuffer sf = new StringBuffer();
		
		if (unit != Unit.NoQuantity){
			if (quantity == (int)quantity)
				sf.append((int)quantity);
			else
				sf.append(quantity);
			
			if (unit == Unit.Gramme || unit == Unit.Litre || unit == Unit.Millilitre)
				sf.append(unit.toString() + " " + I18n.tr("of"));
			else if (unit == Unit.None);
			else
				sf.append(" " + unit.toString() + " " + I18n.tr("of"));
			
			sf.append(" ");
		}
		
		sf.append(name);
		
		if (!comment.isEmpty())
			sf.append(" (" + comment + ")");
		
		return sf.toString();
	}
	
	@Override
	public Object clone() {
		Ingredient i = new Ingredient();
		i.quantity = quantity;
		i.unit = unit;
		i.name = name;
		i.comment = comment;
		return i;
	}
	
	public Element toXmlElement(){
		Element ie = new Element("ingredient");
		
		ie.addContent(new Element("quantity").setText((Double.valueOf(quantity)).toString()));
		ie.addContent(new Element("unit").setText((Integer.valueOf(unit.toInt())).toString()));
		ie.addContent(new Element("name").setText(name));
		ie.addContent(new Element("comment").setText(comment));
		
		return ie;
	}

	
	/**
	 * 
	 */
	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param commentaire the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * 
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((comment == null) ? 0 : comment.hashCode());
		result = (int) (prime * result + quantity);
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ingredient other = (Ingredient) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (comment == null) {
			if (other.comment != null)
				return false;
		} else if (!comment.equals(other.comment))
			return false;
		if (quantity != other.quantity)
			return false;
		if (unit != other.unit)
			return false;
		return true;
	}

}
