package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

public class AboutDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3834005417448307827L;
	private final JPanel contentPanel = new JPanel();
	
	private ActionListener okListener = new ActionListener(){
		public void actionPerformed(ActionEvent arg0) {
			setVisible(false);
		}
	};


	/**
	 * Create the dialog.
	 */
	public AboutDialog(JFrame parent) {
		super(parent,"About RecettesCuisine",true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JLabel lblAbout = new JLabel(I18n.tr("AboutText"));
			lblAbout.setAlignmentX(0.5f);
			contentPanel.add(lblAbout);
		}
		{
			JLabel lblCeProgrammeEst = new JLabel((String) null);
			lblCeProgrammeEst.setAlignmentX(0.5f);
			contentPanel.add(lblCeProgrammeEst);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(okListener);
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		
		setResizable(false);
		pack();
	}

}
