package gui;

import java.util.ResourceBundle;

class I18n {
	private static ResourceBundle messages;

	static {
		messages = ResourceBundle.getBundle("GuiLocale");
	}
	
	static String tr(String key){
		return  messages.getString(key);
	}

}