package gui;

import core.*;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.ListSelectionModel;

public class MainWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1810393566512302281L;
	
	private JPanel contentPane;
	JList<String> list;
	JTextPane txtpnRecipeview;
	JMenuItem mntmSave;
	JMenuItem mntmSaveAs;
	JTabbedPane tabbed_pane;
	JScrollPane edit_scrollPane;
	JButton delete_button;
	
	
	private MenuListener menu_listener;
	
	GuiRecipeList rl;
	RecipeList old_rl;
	
	
	private ListSelectionListener list_listener = new ListSelectionListener(){
		@Override
		public void valueChanged(ListSelectionEvent arg0) {
			if (!arg0.getValueIsAdjusting()){
				int index = list.getSelectedIndex();
				if (index >= 0){
					delete_button.setEnabled(true);
					if (index < rl.getRecipes().size()){
						tabbed_pane.setSelectedIndex(0);
						txtpnRecipeview.setText(rl.getRecipes().get(index).toHTMLString());
					}
					else{
						rl.add(new Recipe(I18n.tr("NewRecipe")));
						if (tabbed_pane.getSelectedIndex() == 1)
							tabbed_pane.setSelectedIndex(0);
						else
							txtpnRecipeview.setText(rl.getRecipes().get(index).toHTMLString());
					}
				}
			}
		}
	};
	
	private ActionListener delete_recipe_listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			int index = list.getSelectedIndex();
			Recipe r = rl.getRecipes().get(index);
			rl.remove(r);
			tabbed_pane.setSelectedIndex(0);
			delete_button.setEnabled(false);
			txtpnRecipeview.setText(I18n.tr("No recipe selected"));
			list.clearSelection();
		}
	};
	
	private WindowAdapter close_window_listener = new WindowAdapter() {
		@Override
        public void windowClosing(WindowEvent e) {
			if (! old_rl.sameRecipes(rl)){
				JOptionPane opt = new JOptionPane(I18n.tr("Save?"),JOptionPane.QUESTION_MESSAGE,JOptionPane.YES_NO_OPTION);
				JDialog jd = opt.createDialog(MainWindow.this, I18n.tr("SaveCurrentFile"));
				jd.setVisible(true);
				if ((Integer)opt.getValue() == 0){
					//To be done
					if (rl.getName() == null){
						JFileChooser save_chooser = new JFileChooser();
						int returnVal2 = save_chooser.showDialog(MainWindow.this, I18n.tr("Save"));
						if(returnVal2 == JFileChooser.APPROVE_OPTION) {
							try {
								rl.save(save_chooser.getSelectedFile().getAbsolutePath());
							} catch (IOException ex) {
								ex.printStackTrace();
								JOptionPane opt_error = new JOptionPane(I18n.tr("FileNotValid"),JOptionPane.ERROR_MESSAGE,JOptionPane.CLOSED_OPTION);
								JDialog jd_error = opt_error.createDialog(MainWindow.this, I18n.tr("Error"));
								jd_error.setVisible(true);
							}
						}
					}else{
						try {
							rl.save();
						} catch (IOException ex) {
							ex.printStackTrace();
							JOptionPane opt_error = new JOptionPane(I18n.tr("FileNotValid"),JOptionPane.ERROR_MESSAGE,JOptionPane.CLOSED_OPTION);
							JDialog jd_error = opt_error.createDialog(MainWindow.this, I18n.tr("Error"));
							jd_error.setVisible(true);
						}
					}
				}
			}
        }
	};
	
	private ChangeListener tabbed_listener;

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		super("RecettesCuisine");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setMinimumSize(new Dimension(700, 500));
		addWindowListener(close_window_listener);
		
		rl = new GuiRecipeList();
		old_rl = (RecipeList)rl.clone();
		
		//
		// Menu
		//
		
		menu_listener = new MenuListener(this);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu(I18n.tr("File"));
		menuBar.add(mnFile);
		
		JMenuItem mntmNew = new JMenuItem(I18n.tr("New"));
		mntmNew.setActionCommand("new");
		mntmNew.addActionListener(menu_listener);
		mnFile.add(mntmNew);
		
		JMenuItem mntmOpen = new JMenuItem(I18n.tr("Open"));
		mntmOpen.setActionCommand("open");
		mntmOpen.addActionListener(menu_listener);
		mnFile.add(mntmOpen);
		
		mntmSave = new JMenuItem(I18n.tr("Save"));
		mntmSave.setActionCommand("save");
		mntmSave.addActionListener(menu_listener);
		mntmSave.setEnabled(false);
		mnFile.add(mntmSave);
		
		mntmSaveAs = new JMenuItem(I18n.tr("Save As"));
		mntmSaveAs.setActionCommand("save_as");
		mntmSaveAs.addActionListener(menu_listener);
		mnFile.add(mntmSaveAs);
		
		JMenuItem mntmClose = new JMenuItem(I18n.tr("Close"));
		mntmClose.setActionCommand("close");
		mntmClose.addActionListener(menu_listener);
		
		JMenuItem mntmExport = new JMenuItem(I18n.tr("Export"));
		mntmExport.setActionCommand("export");
		mntmExport.addActionListener(menu_listener);
		mnFile.add(mntmExport);
		
		mnFile.add(mntmClose);
		
		JMenu mnAbout = new JMenu(I18n.tr("About"));
		menuBar.add(mnAbout);
		
		JMenuItem mntmAbout = new JMenuItem(I18n.tr("About"));
		mntmAbout.setActionCommand("about");
		mntmAbout.addActionListener(menu_listener);
		mnAbout.add(mntmAbout);
		
		//
		// Content
		//
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		//
		// List
		//
		
		JPanel list_panel = new JPanel();
		contentPane.add(list_panel, BorderLayout.WEST);
		list_panel.setLayout(new BoxLayout(list_panel, BoxLayout.Y_AXIS));
		
		JLabel lblHereIsYour = new JLabel(I18n.tr("Here is your recipes"));
		lblHereIsYour.setAlignmentX(0.5f);
		list_panel.add(lblHereIsYour);
		lblHereIsYour.setHorizontalAlignment(SwingConstants.LEFT);
		
		JScrollPane scrollPane = new JScrollPane();
		list_panel.add(scrollPane);
		
		list = new JList<>(rl);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.addListSelectionListener(list_listener);
		scrollPane.setViewportView(list);
		
		delete_button = new JButton(I18n.tr("Delete"));
		delete_button.setAlignmentX(0.5f);
		delete_button.setEnabled(false);
		delete_button.addActionListener(delete_recipe_listener);
		list_panel.add(delete_button);
		
		//
		// Tabben
		//
		
		tabbed_pane = new JTabbedPane(JTabbedPane.TOP);
		tabbed_listener = new TabbedListener(this);
		tabbed_pane.addChangeListener(tabbed_listener);
		contentPane.add(tabbed_pane, BorderLayout.CENTER);
		
		//
		// View
		//
		JScrollPane view_scrollPane = new JScrollPane();
		tabbed_pane.addTab(I18n.tr("View"), null, view_scrollPane, null);
		
		txtpnRecipeview = new JTextPane();
		txtpnRecipeview.setEditable(false);
		txtpnRecipeview.setContentType("text/html");
		txtpnRecipeview.setText(I18n.tr("No recipe selected"));
		view_scrollPane.setViewportView(txtpnRecipeview);
		
		//
		// Edit
		//
		edit_scrollPane = new JScrollPane();
		tabbed_pane.addTab(I18n.tr("Edit"), null, edit_scrollPane, null);
		
		pack();
		setSize(900, 600);
	}

	/**
	 * @return the rl
	 */
	public RecipeList getRl() {
		return rl;
	}

	/**
	 * @param rl the rl to set
	 */
	public void setRl(GuiRecipeList rl) {
		this.rl = rl;
	}

}
