/**
 * 
 */
package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;


import core.*;;

/**
 * @author dalan
 *
 */
public class TabbedListener implements ChangeListener {

	private MainWindow main_window;
	
	private JPanel edit_panel;
	private Recipe r;
	
	//
	// Name
	//
	private JTextField name_textfield;
	
	private DocumentListener name_listener = new DocumentListener(){
		@Override
		public void changedUpdate(DocumentEvent arg0) {
			saveChange();
		}

		@Override
		public void insertUpdate(DocumentEvent arg0) {
			saveChange();
		}

		@Override
		public void removeUpdate(DocumentEvent arg0) {
			saveChange();
		}
		
		private void saveChange(){
			r.setName(name_textfield.getText());
			main_window.rl.notifyChangment();
		}
	};
	
	//
	// Time
	//
	private JSpinner time_spinner;
	private ChangeListener time_listener = new ChangeListener() {
		@Override
		public void stateChanged(ChangeEvent e) {
			r.setTime((Integer)time_spinner.getValue());
		}
	};
	
	//
	// Difficulty
	//
	private JComboBox<Difficulty> difficulty_combobox;
	private ActionListener difficulty_listener = new ActionListener(){
		@Override
		public void actionPerformed(ActionEvent arg0) {
			r.setDifficulty((Difficulty)difficulty_combobox.getSelectedItem());
		}
	};
	
	//
	// Baking
	//
	private JComboBox<Baking> baking_combobox;
	private ActionListener baking_listener = new ActionListener(){
		@Override
		public void actionPerformed(ActionEvent arg0) {
			r.setBaking((Baking)baking_combobox.getSelectedItem());
		}
	};
	
	//
	// Grade
	//
	private JSpinner grade_spinner;
	private ChangeListener grade_listener = new ChangeListener() {
		@Override
		public void stateChanged(ChangeEvent e) {
			r.setGrade((Integer)grade_spinner.getValue());
		}
	};
	
	//
	// Nb person
	//
	private JSpinner nb_person_spinner;
	private ChangeListener nb_person_listener = new ChangeListener() {
		@Override
		public void stateChanged(ChangeEvent e) {
			r.setNb_person((Integer)nb_person_spinner.getValue());
		}
	};
	
	//
	// Ingredients
	//
	static private final int ing_textfield_size = 12;
	private JPanel ingredients_panel;
	private ArrayList<JTextField> ingredient_name_textfields;
	private ArrayList<JTextField> ingredient_note_textfields;
	private ArrayList<JComboBox<Unit>> ingredient_combobox;
	private ArrayList<JSpinner> ingredient_spinner;
	private ArrayList<JButton> ingredients_remove_buttons;
	private ActionListener ingredient_add_listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			Ingredient ing = new Ingredient("");
			r.getIngredients().add(ing);
			JPanel tmp_panel = new JPanel();
			
			JSpinner spin = new JSpinner(new SpinnerNumberModel(0,0,Double.MAX_VALUE,0.1));
			spin.setValue(ing.getQuantity());
			((JSpinner.DefaultEditor) spin.getEditor()).getTextField().setColumns(4);
			spin.addChangeListener(ingredients_spinner_listener);
			ingredient_spinner.add(spin);
			tmp_panel.add(spin);
			
			JComboBox<Unit> combo = new JComboBox<>(Unit.values());
			combo.setSelectedItem(ing.getUnit());
			combo.addActionListener(ingredients_combobox_listener);
			ingredient_combobox.add(combo);
			tmp_panel.add(combo);
			
			JTextField txt = new JTextField();
			txt.setColumns(ing_textfield_size);
			txt.setText(ing.getName());
			txt.setMaximumSize(txt.getPreferredSize());
			txt.getDocument().addDocumentListener(ingredients_name_listener);
			ingredient_name_textfields.add(txt);
			tmp_panel.add(txt);
			
			JTextField txt2 = new JTextField();
			txt2.setColumns(ing_textfield_size);
			txt2.setText(ing.getComment());
			txt2.setMaximumSize(txt2.getPreferredSize());
			txt2.getDocument().addDocumentListener(ingredients_note_listener);
			ingredient_note_textfields.add(txt2);
			tmp_panel.add(txt2);
			
			JButton remove_button = new JButton(I18n.tr("Remove"));
			remove_button.setActionCommand((((Integer)(r.getIngredients().size()-1)).toString()));
			remove_button.addActionListener(ingredient_remove_listener);
			ingredients_remove_buttons.add(remove_button);
			tmp_panel.add(remove_button);
			
			ingredients_panel.add(tmp_panel,r.getIngredients().size());
			
			ingredients_panel.revalidate();
			ingredients_panel.repaint();
		}
	};
	
	private ActionListener ingredient_remove_listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			int index = Integer.parseInt(e.getActionCommand());
			
			ingredient_name_textfields.remove(index);
			ingredient_combobox.remove(index);
			ingredient_note_textfields.remove(index);
			ingredient_spinner.remove(index);
			ingredients_remove_buttons.remove(index);
			
			r.getIngredients().remove(index);
			
			ingredients_panel.remove(index+1);
			ingredients_panel.revalidate();
			ingredients_panel.repaint();
			
			for (int i=0 ; i<r.getIngredients().size() ; i++){
				ingredients_remove_buttons.get(i).setActionCommand(((Integer)i).toString());
			}
		}
	};
	
	private ActionListener ingredients_combobox_listener = new ActionListener(){
		@Override
		public void actionPerformed(ActionEvent arg0) {
			for (int i = 0 ; i<r.getIngredients().size() ; i++){
				r.getIngredients().get(i).setUnit((Unit)ingredient_combobox.get(i).getSelectedItem());
			}
		}
	};
	
	private ChangeListener ingredients_spinner_listener = new ChangeListener(){
		@Override
		public void stateChanged(ChangeEvent e) {
			for (int i = 0 ; i<r.getIngredients().size() ; i++){
				r.getIngredients().get(i).setQuantity((Double)ingredient_spinner.get(i).getValue());
			}
		}
	};
	
	private DocumentListener ingredients_name_listener = new DocumentListener(){
		@Override
		public void changedUpdate(DocumentEvent arg0) {
			saveChange();
		}

		@Override
		public void insertUpdate(DocumentEvent arg0) {
			saveChange();
		}

		@Override
		public void removeUpdate(DocumentEvent arg0) {
			saveChange();
		}
		
		private void saveChange(){
			for (int i = 0 ; i<r.getIngredients().size() ; i++){
				r.getIngredients().get(i).setName(ingredient_name_textfields.get(i).getText());
			}
		}
	};
	
	private DocumentListener ingredients_note_listener = new DocumentListener(){
		@Override
		public void changedUpdate(DocumentEvent arg0) {
			saveChange();
		}

		@Override
		public void insertUpdate(DocumentEvent arg0) {
			saveChange();
		}

		@Override
		public void removeUpdate(DocumentEvent arg0) {
			saveChange();
		}
		
		private void saveChange(){
			for (int i = 0 ; i<r.getIngredients().size() ; i++){
				r.getIngredients().get(i).setComment(ingredient_note_textfields.get(i).getText());
			}
		}
	};
	
	//
	// Ustensils
	//
	static private final int textfield_size = 35;
	private JPanel ustensils_panel;
	private ArrayList<JTextField> ustensils_textfields;
	private ArrayList<JButton> ustensils_remove_buttons;
	private ActionListener ustensils_add_listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			JPanel tmp_panel = new JPanel();
			
			r.getUtensils().add("");
			JTextField txt = new JTextField();
			txt.setColumns(textfield_size);
			txt.setMaximumSize(txt.getPreferredSize());
			txt.getDocument().addDocumentListener(ustensils_listener);
			tmp_panel.add(txt);
			
			JButton remove_button = new JButton(I18n.tr("Remove"));
			remove_button.setActionCommand((((Integer)(r.getUtensils().size()-1)).toString()));
			remove_button.addActionListener(ustensils_remove_listener);
			ustensils_remove_buttons.add(remove_button);
			tmp_panel.add(remove_button);
			
			ustensils_textfields.add(txt);
			ustensils_panel.add(tmp_panel,r.getUtensils().size());
			ustensils_panel.revalidate();
			ustensils_panel.repaint();
		}
	};
	
	private ActionListener ustensils_remove_listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			int index = Integer.parseInt(e.getActionCommand());
			
			ustensils_textfields.remove(index);
			ustensils_remove_buttons.remove(index);
			
			r.getUtensils().remove(index);
			
			ustensils_panel.remove(index+1);
			ustensils_panel.revalidate();
			ustensils_panel.repaint();
			
			for (int i=0 ; i<r.getUtensils().size() ; i++){
				ustensils_remove_buttons.get(i).setActionCommand(((Integer)i).toString());
			}
		}
	};
	
	private DocumentListener ustensils_listener = new DocumentListener(){
		@Override
		public void changedUpdate(DocumentEvent arg0) {
			saveChange();
		}

		@Override
		public void insertUpdate(DocumentEvent arg0) {
			saveChange();
		}

		@Override
		public void removeUpdate(DocumentEvent arg0) {
			saveChange();
		}
		
		private void saveChange(){
			for (int i = 0 ; i<r.getUtensils().size() ; i++){
				r.getUtensils().set(i, ustensils_textfields.get(i).getText());
			}
		}
	};
	
	
	//
	// Actions
	//
	private JPanel actions_panel;
	private ArrayList<JTextField> actions_textfields;
	private ArrayList<JButton> actions_remove_buttons;
	private ActionListener actions_add_listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			JPanel tmp_panel = new JPanel();
			
			r.getActions().add("");
			JTextField txt = new JTextField();
			txt.setColumns(textfield_size);
			txt.setMaximumSize(new Dimension(Integer.MAX_VALUE,(int) txt.getPreferredSize().getHeight()));
			txt.getDocument().addDocumentListener(actions_listener);
			tmp_panel.add(txt);
			
			JButton remove_button = new JButton(I18n.tr("Remove"));
			remove_button.setActionCommand((((Integer)(r.getActions().size()-1)).toString()));
			remove_button.addActionListener(actions_remove_listener);
			actions_remove_buttons.add(remove_button);
			tmp_panel.add(remove_button);
			
			actions_textfields.add(txt);
			actions_panel.add(tmp_panel,r.getActions().size());
			actions_panel.revalidate();
			actions_panel.repaint();
		}
	};
	
	private ActionListener actions_remove_listener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			int index = Integer.parseInt(e.getActionCommand());
			
			actions_textfields.remove(index);
			actions_remove_buttons.remove(index);
			
			r.getActions().remove(index);
			
			actions_panel.remove(index+1);
			actions_panel.revalidate();
			actions_panel.repaint();
			
			for (int i=0 ; i<r.getActions().size() ; i++){
				actions_remove_buttons.get(i).setActionCommand(((Integer)i).toString());
			}
		}
	};
	
	private DocumentListener actions_listener = new DocumentListener(){
		@Override
		public void changedUpdate(DocumentEvent arg0) {
			saveChange();
		}

		@Override
		public void insertUpdate(DocumentEvent arg0) {
			saveChange();
		}

		@Override
		public void removeUpdate(DocumentEvent arg0) {
			saveChange();
		}
		
		private void saveChange(){
			for (int i = 0 ; i<r.getActions().size() ; i++){
				r.getActions().set(i, actions_textfields.get(i).getText());
			}
		}
	};
	
	
	//
	//
	//
	/**
	 * 
	 */
	public TabbedListener(MainWindow mw) {
		main_window = mw;
	}

	/* (non-Javadoc)
	 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
	 */
	@Override
	public void stateChanged(ChangeEvent e) {
		int list_index = main_window.list.getSelectedIndex();
		if (main_window.tabbed_pane.getSelectedIndex() == 1 && list_index >= 0 ){
			
			r = main_window.rl.getRecipes().get(list_index);
			
			edit_panel = new JPanel();
			edit_panel.setLayout(new BoxLayout(edit_panel, BoxLayout.PAGE_AXIS));
			//edit_panel.setLayout(new GridLayout(7,1));
			main_window.edit_scrollPane.setViewportView(edit_panel);
			
			//
			// Name
			//
			JPanel name_panel = new JPanel();
			name_panel.add(new JLabel(I18n.tr("Name")));
			name_textfield = new JTextField();
			name_textfield.setColumns(30);
			name_textfield.setText(r.getName());
			name_textfield.getDocument().addDocumentListener(name_listener);
			name_panel.add(name_textfield);
			edit_panel.add(name_panel);
			
			//
			// Preparation time
			//
			JPanel time_panel = new JPanel();
			time_panel.add(new JLabel(I18n.tr("PreparationTime")));
			time_spinner = new JSpinner(new SpinnerNumberModel(0,0,Integer.MAX_VALUE,1));
			time_spinner.setValue(r.getTime());
			((JSpinner.DefaultEditor) time_spinner.getEditor()).getTextField().setColumns(5);
			time_spinner.addChangeListener(time_listener);
			time_panel.add(time_spinner);
			edit_panel.add(time_panel);
			
			//
			// Difficulty
			//
			JPanel difficulty_panel = new JPanel();
			difficulty_panel.add(new JLabel(I18n.tr("Difficulty")));
			difficulty_combobox = new JComboBox<>(Difficulty.values());
			difficulty_combobox.setSelectedItem(r.getDifficulty());
			difficulty_combobox.addActionListener(difficulty_listener);
			difficulty_panel.add(difficulty_combobox);
			edit_panel.add(difficulty_panel);
			
			//
			// Grade
			//
			JPanel grade_panel = new JPanel();
			grade_panel.add(new JLabel(I18n.tr("Grade")));
			grade_spinner = new JSpinner(new SpinnerNumberModel(0,0,10,1));
			grade_spinner.setValue(r.getGrade());
			((JSpinner.DefaultEditor) grade_spinner.getEditor()).getTextField().setColumns(5);
			grade_spinner.addChangeListener(grade_listener);
			grade_panel.add(grade_spinner);
			edit_panel.add(grade_panel);
			
			//
			// Baking
			//
			JPanel baking_panel = new JPanel();
			baking_panel.add(new JLabel(I18n.tr("Baking")));
			baking_combobox = new JComboBox<>(Baking.values());
			baking_combobox.setSelectedItem(r.getBaking());
			baking_combobox.addActionListener(baking_listener);
			baking_panel.add(baking_combobox);
			edit_panel.add(baking_panel);
			
			//
			// Nb person
			//
			JPanel nb_person_panel = new JPanel();
			nb_person_panel.add(new JLabel(I18n.tr("NbPerson")));
			nb_person_spinner = new JSpinner(new SpinnerNumberModel(0,0,50,1));
			nb_person_spinner.setValue(r.getNb_person());
			((JSpinner.DefaultEditor) nb_person_spinner.getEditor()).getTextField().setColumns(5);
			nb_person_spinner.addChangeListener(nb_person_listener);
			nb_person_panel.add(nb_person_spinner);
			edit_panel.add(nb_person_panel);
			
			//
			// Ingredients
			//
			ingredients_panel = new JPanel();
			ingredients_panel.setLayout(new BoxLayout(ingredients_panel, BoxLayout.PAGE_AXIS));
			JLabel ingredients_label = new JLabel(I18n.tr("Ingredients"));
			ingredients_label.setAlignmentX(JComponent.CENTER_ALIGNMENT);
			ingredients_panel.add(ingredients_label);
			ingredient_name_textfields = new ArrayList<>();
			ingredient_note_textfields = new ArrayList<>();
			ingredient_spinner = new ArrayList<>();
			ingredient_combobox = new ArrayList<>();
			ingredients_remove_buttons = new ArrayList<>();
			for (int i=0 ; i<r.getIngredients().size() ; i++){
				Ingredient ing = r.getIngredients().get(i);
				JPanel tmp_panel = new JPanel();
				
				JSpinner spin = new JSpinner(new SpinnerNumberModel(0,0,Double.MAX_VALUE,0.1));
				spin.setValue(ing.getQuantity());
				((JSpinner.DefaultEditor) spin.getEditor()).getTextField().setColumns(4);
				spin.addChangeListener(ingredients_spinner_listener);
				ingredient_spinner.add(spin);
				tmp_panel.add(spin);
				
				JComboBox<Unit> combo = new JComboBox<>(Unit.values());
				combo.setSelectedItem(ing.getUnit());
				combo.addActionListener(ingredients_combobox_listener);
				ingredient_combobox.add(combo);
				tmp_panel.add(combo);
				
				JTextField txt = new JTextField();
				txt.setColumns(ing_textfield_size);
				txt.setText(ing.getName());
				txt.setMaximumSize(txt.getPreferredSize());
				txt.getDocument().addDocumentListener(ingredients_name_listener);
				ingredient_name_textfields.add(txt);
				tmp_panel.add(txt);
				
				JTextField txt2 = new JTextField();
				txt2.setColumns(ing_textfield_size);
				txt2.setText(ing.getComment());
				txt2.setMaximumSize(txt2.getPreferredSize());
				txt2.getDocument().addDocumentListener(ingredients_note_listener);
				ingredient_note_textfields.add(txt2);
				tmp_panel.add(txt2);
				
				JButton remove_button = new JButton(I18n.tr("Remove"));
				remove_button.setActionCommand((((Integer)i).toString()));
				remove_button.addActionListener(ingredient_remove_listener);
				ingredients_remove_buttons.add(remove_button);
				tmp_panel.add(remove_button);
				
				ingredients_panel.add(tmp_panel);
			}
			JButton add_ingredients = new JButton(I18n.tr("Add"));
			add_ingredients.setAlignmentX(JComponent.CENTER_ALIGNMENT);
			add_ingredients.addActionListener(ingredient_add_listener);
			ingredients_panel.add(add_ingredients);
			edit_panel.add(ingredients_panel);
			
			//
			// Utensils
			//
			ustensils_panel = new JPanel();
			ustensils_panel.setLayout(new BoxLayout(ustensils_panel, BoxLayout.PAGE_AXIS));
			JLabel ustensil_label = new JLabel(I18n.tr("Ustensils"));
			ustensil_label.setAlignmentX(JComponent.CENTER_ALIGNMENT);
			ustensils_panel.add(ustensil_label);
			ustensils_textfields = new ArrayList<>();
			ustensils_remove_buttons = new ArrayList<>();
			for (int i=0 ; i<r.getUtensils().size() ; i++){
				JPanel tmp_panel = new JPanel();
				
				JTextField txt = new JTextField();
				txt.setColumns(textfield_size);
				txt.setText(r.getUtensils().get(i));
				txt.setMaximumSize(txt.getPreferredSize());
				txt.getDocument().addDocumentListener(ustensils_listener);
				ustensils_textfields.add(txt);
				tmp_panel.add(txt);
				
				JButton remove_button = new JButton(I18n.tr("Remove"));
				remove_button.setActionCommand((((Integer)i).toString()));
				remove_button.addActionListener(ustensils_remove_listener);
				ustensils_remove_buttons.add(remove_button);
				tmp_panel.add(remove_button);
				
				ustensils_panel.add(tmp_panel);
			}
			JButton add_ustensil = new JButton(I18n.tr("Add"));
			add_ustensil.setAlignmentX(JComponent.CENTER_ALIGNMENT);
			add_ustensil.addActionListener(ustensils_add_listener);
			ustensils_panel.add(add_ustensil);
			edit_panel.add(ustensils_panel);
			
			
			//
			// Actions
			//
			actions_panel = new JPanel();
			actions_panel.setLayout(new BoxLayout(actions_panel, BoxLayout.PAGE_AXIS));
			JLabel actions_label = new JLabel(I18n.tr("Actions"));
			actions_label.setAlignmentX(JComponent.CENTER_ALIGNMENT);
			actions_panel.add(actions_label);
			actions_textfields = new ArrayList<>();
			actions_remove_buttons = new ArrayList<>();
			for (int i=0 ; i<r.getActions().size() ; i++){
				JPanel tmp_panel = new JPanel();
				
				JTextField txt = new JTextField();
				txt.setText(r.getActions().get(i));
				txt.setColumns(textfield_size);
				txt.setMaximumSize(new Dimension(Integer.MAX_VALUE,(int) txt.getPreferredSize().getHeight()));
				txt.getDocument().addDocumentListener(actions_listener);
				actions_textfields.add(txt);
				tmp_panel.add(txt);
				
				JButton remove_button = new JButton(I18n.tr("Remove"));
				remove_button.setActionCommand((((Integer)i).toString()));
				remove_button.addActionListener(actions_remove_listener);
				actions_remove_buttons.add(remove_button);
				tmp_panel.add(remove_button);
				
				actions_panel.add(tmp_panel);
			}
			JButton add_actions = new JButton(I18n.tr("Add"));
			add_actions.setAlignmentX(JComponent.CENTER_ALIGNMENT);
			add_actions.addActionListener(actions_add_listener);
			actions_panel.add(add_actions);
			edit_panel.add(actions_panel);
			
		} else {
			if (list_index >= 0)
				main_window.txtpnRecipeview.setText(main_window.rl.getRecipes().get(list_index).toHTMLString());
			else if (main_window.txtpnRecipeview != null)
				main_window.txtpnRecipeview.setText(I18n.tr("No recipe selected"));
			
			if (edit_panel!= null)
				edit_panel.removeAll();
		}
	}

}
