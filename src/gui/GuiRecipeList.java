/**
 * 
 */
package gui;

import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import core.*;

/**
 * @author dalan
 *
 */
public class GuiRecipeList extends RecipeList implements ListModel<String> {
	
	protected ArrayList<ListDataListener> listener_list;
	protected ArrayList<String> recipes_names;

	/**
	 * 
	 */
	private static final long serialVersionUID = 3632348981899074666L;

	/**
	 * 
	 */
	public GuiRecipeList() {
		super();
		listener_list = new ArrayList<>();
		recipes_names = new ArrayList<>();
		recipes_names.add(I18n.tr("New recipe"));
	}

	@Override
	public void addListDataListener(ListDataListener l) {
		listener_list.add(l);
	}

	@Override
	public String getElementAt(int index) {
		return recipes_names.get(index);
	}

	@Override
	public int getSize() {
		return recipes_names.size();
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
		listener_list.remove(l);
	}
	
	@Override
	public void clear(){
		super.clear();
		recipes_names.clear();
		recipes_names.add(I18n.tr("New recipe"));
		notifyChangment();
	}
	
	@Override
	public void open(String n) throws IOException{
		super.open(n);
		recipes_names.clear();
		for (Recipe r : recipes)
			recipes_names.add(r.getName());
		recipes_names.add(I18n.tr("New recipe"));
		notifyChangment();
	}
	
	public void add(Recipe r){
		recipes.add(r);
		recipes_names.remove(I18n.tr("New recipe"));
		recipes_names.add(r.getName());
		recipes_names.add(I18n.tr("New recipe"));
		notifyChangment();
	}
	
	public void remove(Recipe r){
		recipes.remove(r);
		recipes_names.remove(r.getName());
		notifyChangment();
	}

	
	public void notifyChangment(){
		int i = 0;
		for (Recipe r : recipes){
			if (r.getName() != recipes_names.get(i)){
				recipes_names.remove(i);
				recipes_names.add(i, r.getName());
			}
			i++;
		}
		for (ListDataListener l : listener_list)
			l.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, 0));
	}
}
