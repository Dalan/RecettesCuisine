/**
 * 
 */
package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import core.RecipeList;

/**
 * @author dalan
 *
 */
public class MenuListener implements ActionListener {

	private MainWindow main_window;
	private AboutDialog about_dialog;
	
	private FileFilter html_file_filter = new FileFilter(){

		@Override
		public boolean accept(File arg0) {
			if (arg0.isDirectory())
				return true;
			if (arg0.getName().endsWith(".html"))
				return true;
			return false;
		}

		@Override
		public String getDescription() {
			return I18n.tr("html_file");
		}
		
	};
	
	private FileFilter markdown_file_filter = new FileFilter(){

		@Override
		public boolean accept(File arg0) {
			if (arg0.isDirectory())
				return true;
			else if (arg0.getName().endsWith(".md"))
				return true;
			else if (arg0.getName().endsWith(".markdown"))
				return true;
			return false;
		}

		@Override
		public String getDescription() {
			return I18n.tr("markdown_file");
		}
		
	};
	
	private FileFilter xml_file_filter = new FileFilter(){

		@Override
		public boolean accept(File arg0) {
			if (arg0.isDirectory())
				return true;
			if (arg0.getName().endsWith(".xml"))
				return true;
			return false;
		}

		@Override
		public String getDescription() {
			return I18n.tr("xml_file");
		}
		
	};
	
	private FileFilter json_file_filter = new FileFilter(){

		@Override
		public boolean accept(File arg0) {
			if (arg0.isDirectory())
				return true;
			return false;
		}

		@Override
		public String getDescription() {
			return I18n.tr("json_file");
		}
		
	};
	
	private FileFilter pdf_file_filter = new FileFilter(){

		@Override
		public boolean accept(File arg0) {
			if (arg0.isDirectory())
				return true;
			if (arg0.getName().endsWith(".pdf"))
				return true;
			return false;
		}

		@Override
		public String getDescription() {
			return I18n.tr("pdf_file");
		}
		
	};
	
	/**
	 * 
	 */
	public MenuListener(MainWindow mw) {
		main_window = mw;
		
		about_dialog = new AboutDialog(mw);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "about":
			about_dialog.setLocationRelativeTo(main_window);
			about_dialog.setVisible(true);
			break;

		case "close":
			main_window.setVisible(false);
			break;

		case "new":
			main_window.rl.clear();
			main_window.old_rl = (RecipeList)main_window.rl.clone();
			main_window.mntmSaveAs.setEnabled(true);
			main_window.mntmSave.setEnabled(false);
			main_window.txtpnRecipeview.setText(I18n.tr("No recipe selected"));
			main_window.list.clearSelection();
			main_window.tabbed_pane.setSelectedIndex(0);
			main_window.delete_button.setEnabled(false);
			break;
			
		case "open":
			JFileChooser chooser = new JFileChooser();
			int returnVal = chooser.showDialog(main_window, I18n.tr("Open"));
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				try {
					main_window.rl.open(chooser.getSelectedFile().getAbsolutePath());
					main_window.old_rl = (RecipeList)main_window.rl.clone();
					main_window.mntmSave.setEnabled(true);
					main_window.mntmSaveAs.setEnabled(true);
					main_window.txtpnRecipeview.setText(I18n.tr("No recipe selected"));
					main_window.list.clearSelection();
					main_window.delete_button.setEnabled(false);
				} catch (IOException ex) {
					ex.printStackTrace();
					JOptionPane opt = new JOptionPane(I18n.tr("FileNotValid"),JOptionPane.ERROR_MESSAGE,JOptionPane.CLOSED_OPTION);
					JDialog jd = opt.createDialog(main_window, I18n.tr("Error"));
					jd.setVisible(true);
					main_window.mntmSave.setEnabled(false);
				}
			}
			break;

		case "save":
			try {
				main_window.rl.save();
				main_window.old_rl = (RecipeList)main_window.rl.clone();
			} catch (IOException ex) {
				ex.printStackTrace();
				JOptionPane opt = new JOptionPane(I18n.tr("FileNotValid"),JOptionPane.ERROR_MESSAGE,JOptionPane.CLOSED_OPTION);
				JDialog jd = opt.createDialog(main_window, I18n.tr("Error"));
				jd.setVisible(true);
			}
			break;

		case "save_as":
			JFileChooser save_chooser = new JFileChooser();
			int returnVal2 = save_chooser.showDialog(main_window, I18n.tr("Save"));
			if(returnVal2 == JFileChooser.APPROVE_OPTION) {
				try {
					main_window.rl.save(save_chooser.getSelectedFile().getAbsolutePath());
					main_window.old_rl = (RecipeList)main_window.rl.clone();
					main_window.mntmSave.setEnabled(true);
				} catch (IOException ex) {
					ex.printStackTrace();
					JOptionPane opt = new JOptionPane(I18n.tr("FileNotValid"),JOptionPane.ERROR_MESSAGE,JOptionPane.CLOSED_OPTION);
					JDialog jd = opt.createDialog(main_window, I18n.tr("Error"));
					jd.setVisible(true);
				}
			}
			break;
			
		case "export":
			JFileChooser export_chooser = new JFileChooser();
			export_chooser.setAcceptAllFileFilterUsed(false);
			export_chooser.addChoosableFileFilter(html_file_filter);
			export_chooser.addChoosableFileFilter(markdown_file_filter);
			export_chooser.addChoosableFileFilter(xml_file_filter);
			export_chooser.addChoosableFileFilter(pdf_file_filter);
			export_chooser.addChoosableFileFilter(json_file_filter);
			export_chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			int returnVal3 = export_chooser.showDialog(main_window, I18n.tr("Export"));
			if(returnVal3 == JFileChooser.APPROVE_OPTION) {
				File selected_file = export_chooser.getSelectedFile();
				if (selected_file.exists() && selected_file.isFile())
				{
					JOptionPane opt = new JOptionPane(I18n.tr("Overwrite?") + "(" + selected_file.getAbsolutePath() + ")",JOptionPane.QUESTION_MESSAGE,JOptionPane.YES_NO_OPTION);
					JDialog jd = opt.createDialog(main_window, I18n.tr("OverwriteFile"));
					jd.setVisible(true);
					if (opt.getValue() == (Integer)JOptionPane.NO_OPTION)
						break;
				}
				
				try {
					if (export_chooser.getFileFilter().equals(json_file_filter)) {
						if (!selected_file.isDirectory()) {
							JOptionPane opt = new JOptionPane(I18n.tr("ExportErrorNeedFolder"),JOptionPane.ERROR_MESSAGE,JOptionPane.CLOSED_OPTION);
							JDialog jd = opt.createDialog(main_window, I18n.tr("Error"));
							jd.setVisible(true);
							break;
						}
						main_window.rl.exportToJson(selected_file.getAbsolutePath());
					} else {
						if (selected_file.exists() && !selected_file.isFile()) {
							JOptionPane opt = new JOptionPane(I18n.tr("ExportErrorNeedFile"),JOptionPane.ERROR_MESSAGE,JOptionPane.CLOSED_OPTION);
							JDialog jd = opt.createDialog(main_window, I18n.tr("Error"));
							jd.setVisible(true);
							break;
						}

						if (export_chooser.getFileFilter().equals(html_file_filter))
							main_window.rl.exportToHtml(selected_file.getAbsolutePath());
						else if (export_chooser.getFileFilter().equals(xml_file_filter))
							main_window.rl.exportToXml(selected_file.getAbsolutePath());
						else if (export_chooser.getFileFilter().equals(markdown_file_filter))
							main_window.rl.exportToMarkdown(selected_file.getAbsolutePath());
						else
							main_window.rl.exportToPdf(selected_file.getAbsolutePath());
					}
				} catch (IOException ex) {
					ex.printStackTrace();
					JOptionPane opt = new JOptionPane(I18n.tr("FileNotValid"),JOptionPane.ERROR_MESSAGE,JOptionPane.CLOSED_OPTION);
					JDialog jd = opt.createDialog(main_window, I18n.tr("Error"));
					jd.setVisible(true);
				} catch (Exception ex) {
					ex.printStackTrace();
					JOptionPane opt = new JOptionPane(I18n.tr("ExportError"),JOptionPane.ERROR_MESSAGE,JOptionPane.CLOSED_OPTION);
					JDialog jd = opt.createDialog(main_window, I18n.tr("Error"));
					jd.setVisible(true);
				}
			}
			break;

			
		default:
			break;
		}
	}

}
