package core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestRecipe {
	
	private Recipe r;

	@Before
	public void setUp() {
		r = new Recipe("Test");
		r.setBaking(Baking.None);
		r.setDifficulty(Difficulty.Hard);
		r.setGrade(5);
		r.setTime(20);
		r.getActions().add("Test");
		r.getActions().add("Blop");
		r.getIngredients().add(new Ingredient());
		r.getUtensils().add("Blip");
	}

	@Test
	public void testClone() {
		Recipe r_cl = (Recipe) r.clone();
		assertTrue(r_cl.equals(r));
	}

}
