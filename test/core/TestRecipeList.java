package core;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class TestRecipeList {
	
	private RecipeList rl;
	private String tmp_name;

	@Before
	public void setUp() {
		tmp_name = System.getProperty("java.io.tmpdir") + "/test_recipe_list";
		
		rl = new RecipeList();
		rl.setName(tmp_name);
		
		Recipe r1 = new Recipe("Blop");
		r1.setBaking(Baking.None);
		r1.setDifficulty(Difficulty.Hard);
		r1.setGrade(5);
		r1.setTime(20);
		r1.getActions().add("Test");
		r1.getActions().add("Blop");
		r1.getIngredients().add(new Ingredient());
		r1.getUtensils().add("Blip");
		
		Recipe r2 = new Recipe("Trey");
		r2.setBaking(Baking.Oven);
		r2.setDifficulty(Difficulty.VeryEasy);
		r2.setGrade(7);
		r2.setTime(12);
		r2.getActions().add("Hi");
		r2.getIngredients().add(new Ingredient());
		r2.getIngredients().add(new Ingredient());
		r2.getUtensils().add("Plop");
		r2.getUtensils().add("World");
		
		rl.getRecipes().add(r1);
		rl.getRecipes().add(r2);
	}

	@Test
	public void testClone() {
		RecipeList rl_cl = (RecipeList) rl.clone();
		assertTrue(rl_cl.equals(rl));
	}

	@Test
	public void testSaveOpen() throws IOException {
		rl.save();
		RecipeList rl2 = new RecipeList();
		rl2.open(tmp_name);
		assertTrue(rl2.equals(rl));
	}

	@Test
	public void testSameRecipes() {
		RecipeList rl_cl = (RecipeList) rl.clone();
		assertTrue(rl_cl.sameRecipes(rl));
	}

	@Test
	public void testSameRecipesFalse() {
		RecipeList rl_cl = (RecipeList) rl.clone();
		rl_cl.getRecipes().get(0).setGrade(1);
		assertFalse(rl_cl.sameRecipes(rl));
	}

}
