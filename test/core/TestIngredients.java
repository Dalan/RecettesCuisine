package core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class TestIngredients {
	
	private Ingredient ing;
	
	@Before
	public void setUp() {
		ing = new Ingredient();
	}

	@Test
	public void testClone() {
		Ingredient ing_cl = (Ingredient) ing.clone();
		assertTrue(ing_cl.equals(ing));
	}

}
