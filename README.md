Recettes de cuisine
===================

This is a recipe software. It is design to store classical recipe on a computer.

Future
-------

I created it in a student project and I don't want to continue it after.
But the program work and I will probably create another recipe software in some time which can import the data of this software. And of course if someone found a bug, I will fixed it :)

Functionalities
----------------
The main functionnalities are:

 * Save recipe with grade, ingredients, utensils... in a file with java serialization
 * Export it to HTML, PDF, Markdown and XML
 * Has a view and edit screen
 
Build
-----
You can import the project in Eclipse to build it.

Ant is used to create the jar file.
```
ant -f ant_jar.xml create_run_jar
java -jar RecettesCuisine.jar
```

Download
--------
You can download the all in one jar file [here](https://www.binaries.dalan.fr/RecettesCuisine/RecettesCuisine.jar).

Screenshot
----------
![View screen](View_screen.png)

![Edit screen](Edit_screen.png)